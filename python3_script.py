#! C:/python.exe
import os
import subprocess
import coverage
import sys

os.environ['COVERAGE_PROCESS_START'] = 'C:\Lib\site-packages\.coveragerc'

def test_method():
    sum = 0
    for i in range(100):
        sum +=i
    print("dummy method output is : {}".format(sum))
    call_python2()


def call_python2():
    subprocess.call(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'python2_script.py'), shell=True)


if __name__ == '__main__':
    print("python3 - v {}".format(sys.version))
    cov = coverage.coverage()
    cov.start()
    test_method()
    cov.stop()
    cov.combine()
    cov.html_report()
    cov.save()
